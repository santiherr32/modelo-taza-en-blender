# Modelo Taza en Blender

## Nombre
Servicio de visualización de diseños para tazas

## Descripción
El proyecto consiste en un pequeño servicio de visualización del diseño que el cliente elija, sobre el modelo de una taza hecho en la herramienta Blender.

## Objetivos

### General: 
Proveer el servicio de visualización de diseños a través del modelado tridimensional de tasas.

### Especificos:
1.	Aprovechar las herramientas digitales y de software libre que permiten ofrecer el servicio de modelado para su uso comercial.
2.	Generar una pequeña fuente de ingresos a traves de la prestación de este servicio.

## Alcance
Este proyecto pretende permitir el acceso al servicio ya descrito especificamente a clientes de pequeñas o medianas empresas principalmente dedicadas al negocio de fabricación de tasas con cualquier proposito. Además, el mercado de este pequeño servicio incluye a personas comunes que necesiten del mismo para sus propositos personales. Por ultimo, cabe aclarar que este servicio no asume la responsabilidad del uso que den los clientes al modelo entregado y que los diseños que elijan no deben incumplir ningun tipo de ley de derechos de autor o de otro tipo.

## Condicionales
- Se prefiere el uso de imagenes en formato .png para las imagenes que se usen de estampada sobre el modelo, dada su transparencia.
- El tamaño de la imagen no debe ser muy grande, al menos no > 1500 px * 1500px debido a que la imagen sera reescalada a menos de 1800px * 1800px para encajar en el espacio designado para el estampado en el modelo.

## Futuras mejoras
- Probar mas posibles diseños para el modelo de taza

## Autor
Santiago Herrera Rocha.

## License
[MIT](https://choosealicense.com/licenses/mit/)

